import React, { useState } from 'react';
import {
  Text,
  View,
  TextInput,
  ImageBackground,
  Image,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Spinner } from 'native-base';

import colors from '../../theme/colors';
import styles from './styles';
import RoundButton from '../../components/RoundButton';
import {
  emailAndPasswordSignIn,
  googleSignIn,
  getCurrentUser
} from '../../services/authService';
import { listenNotifications } from '../../services/notificationsService';

const Login = props => {
  const [emailLoading, setEmailLoading] = useState(false);
  const [socialLoading, setSocialLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const signIn = () => {
    setEmailLoading(true);
    emailAndPasswordSignIn(email, password)
      .then(() => {
        const uid = getCurrentUser().uid;
        listenNotifications(uid);
        setEmailLoading(false);
        props.navigation.navigate('User');
      })
      .catch(err => {
        console.log(JSON.stringify(err));
        setEmailLoading(false);
        if (err.code == 'auth/wrong-password') {
          Alert.alert('Erro', 'Senha incorreta');
        } else if (err.code == 'auth/user-not-found') {
          Alert.alert('Erro', 'Email não cadastrado');
        } else {
          Alert.alert('Erro', 'Ocorreu um erro. Tente novamente.');
        }
      });
  };

  const socialSignIn = () => {
    setSocialLoading(true);
    googleSignIn()
      .then(() => {
        const uid = getCurrentUser().uid;
        listenNotifications(uid);
        setSocialLoading(false);
        props.navigation.navigate('User');
      })
      .catch(err => {
        setSocialLoading(false);
        console.log('Social login error', err);
        Alert.alert('Erro', 'Ocorreu um erro. Tente novamente.');
      });
  };

  return (
    <ImageBackground
      style={styles.viewport}
      source={require('../../theme/assets/login-background/background.png')}
      resizeMode={'cover'}
    >
      <View style={styles.container}>
        <View style={styles.logoHolder}>
          <Image
            style={styles.firebaseLogo}
            source={require('../../theme/assets/firebase-logo/firebase.png')}
            resizeMode={'contain'}
          />
        </View>

        <View style={styles.inputsHolder}>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Email'}
            autoCompleteType={'email'}
            keyboardType={'email-address'}
            textContentType={'emailAddress'}
            autoCapitalize={'none'}
            onChangeText={text => setEmail(text)}
          ></TextInput>

          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Senha'}
            textContentType={'password'}
            secureTextEntry={true}
            onChangeText={text => setPassword(text)}
          ></TextInput>
        </View>

        <View style={styles.buttonsHolder}>
          <RoundButton
            backgroundColor={'#FFFFFF'}
            borderColor={'transparent'}
            onPress={signIn}
          >
            {emailLoading ? (
              <Spinner color={colors.primary} />
            ) : (
              <Text style={styles.textFullButton}>LOGIN</Text>
            )}
          </RoundButton>
          <RoundButton
            backgroundColor={'transparent'}
            borderColor={'#FFFFFF'}
            onPress={socialSignIn}
          >
            {socialLoading ? (
              <Spinner color={'#FFFFFF'} />
            ) : (
              <Icon
                name={'google-plus'}
                style={[styles.textOutlineButton, styles.socialIcon]}
              />
            )}
          </RoundButton>
          <RoundButton
            backgroundColor={'transparent'}
            borderColor={'transparent'}
            onPress={() => {
              props.navigation.navigate('SignUp');
            }}
          >
            <Text style={styles.textOutlineButton}>
              Não possui uma conta?
              <Text style={{ fontWeight: 'bold' }}> Cadastre-se!</Text>
            </Text>
          </RoundButton>
        </View>
      </View>
    </ImageBackground>
  );
};

export default Login;
