import { StyleSheet, Dimensions } from 'react-native';
import variables from '../../theme/variables';
import colors from '../../theme/colors';

const deviceHeigth = Dimensions.get('window').height;

export default StyleSheet.create({
  viewport: {
    height: deviceHeigth
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: variables.pagePadding
  },
  logoHolder: {
    flex: 0.3
  },
  firebaseLogo: {
    height: '85%'
  },
  inputsHolder: {
    flex: 0.3,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  textInput: {
    width: '100%',
    color: '#FFFFFF',
    paddingBottom: 20
  },
  buttonsHolder: {
    flex: 0.4,
    flexDirection: 'column',
    alignSelf: 'stretch',
    justifyContent: 'flex-end'
  },
  textFullButton: {
    fontWeight: 'bold',
    color: colors.primary
  },
  textOutlineButton: {
    color: '#FFFFFF'
  },
  socialIcon: {
    fontSize: 22
  }
});
