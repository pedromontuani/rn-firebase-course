import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import {
  Container,
  Header,
  Body,
  Title,
  Right,
  Button,
  Icon,
  Fab
} from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { SwipeListView } from 'react-native-swipe-list-view';
import moment from 'moment';

import styles from './styles';
import colors from '../../theme/colors';
import ChatItem from '../../components/ChatItem';
import { signOut, getCurrentUser } from '../../services/authService';
import { getChats, getContact } from '../../services/firestoreService';

const Home = props => {
  const [chats, setChats] = useState([]);
  let unsubscribe = undefined;

  useEffect(() => {
    unsubscribe = getChats(getCurrentUser().uid).onSnapshot(data => {
      setChats(data.docs);
    });
  }, []);

  useEffect(() => {
    return () => {
      unsubscribe();
    };
  }, []);

  const SignOut = () => {
    signOut().then(() => {
      props.navigation.navigate('Auth');
    });
  };

  const extractTimestamp = timestamp => {
    const fullTime = timestamp.toDate();

    if (new Date().toDateString() == fullTime.toDateString()) {
      return moment(fullTime).format('HH:mm');
    } else {
      return moment(fullTime).format('DD/MM/YY HH:mm');
    }
  };

  return (
    <Container>
      <Header
        noLeft
        androidStatusBarColor={colors.primaryDark}
        style={styles.header}
      >
        <Body>
          <Title>Chats</Title>
        </Body>
        <Right>
          <TouchableNativeFeedback>
            <Button transparent onPress={SignOut}>
              <Icon name={'sign-out'} type={'FontAwesome'} />
            </Button>
          </TouchableNativeFeedback>
        </Right>
      </Header>

      <View style={styles.content}>
        <SwipeListView
          useFlatList
          disableRightSwipe
          rightOpenValue={-60}
          data={chats}
          contentContainerStyle={styles.chatsList}
          keyExtractor={item => item.id}
          renderItem={({ item }) => (
            <ChatItem
              contactUid={item.id}
              subtitle={item.data().lastMessage.message}
              timestamp={extractTimestamp(item.data().lastMessage.timestamp)}
              props={props}
            />
          )}
          renderHiddenItem={({ item }) => (
            <Button
              full
              danger
              iconRight
              onPress={() => {
                alert('kjdskj');
              }}
              style={styles.deletionButton}
            >
              <Icon active name="trash" />
            </Button>
          )}
        />
      </View>

      <Fab
        style={styles.fab}
        onPress={() => props.navigation.navigate('Contacts')}
      >
        <Icon name="plus" type="FontAwesome" />
      </Fab>
    </Container>
  );
};

export default Home;
