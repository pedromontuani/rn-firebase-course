import { StyleSheet } from 'react-native';
import colors from '../../theme/colors';

export default StyleSheet.create({
  header: {
      backgroundColor: colors.primary
  },
  content: {
    flex: 1
  },
  searchBox: {
    backgroundColor: '#555555',
    height: 50,
  },
  chatsList: {
    paddingBottom: 90
  },
  deletionButton: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: '100%',
    paddingRight: 5
  },
  fab: {
    backgroundColor: colors.secondary
  }
});
