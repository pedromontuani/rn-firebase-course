import React, { useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';

import { getAuth } from '../../services/authService';
import { listenNotifications } from '../../services/notificationsService';

const Startup = props => {
  let unsubscribe = undefined;

  useEffect(() => {
    unsubscribe = getAuth().onAuthStateChanged(authState => {
      if (authState) {
        listenNotifications(authState.uid).catch(err => {
          console.log('Notifications setup error', err);
        });
        props.navigation.navigate('User');
      } else {
        props.navigation.navigate('Auth');
      }
    });
  }, []);

  useEffect(() => {
    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
      }}
    >
      <ActivityIndicator />
    </View>
  );
};

export default Startup;
