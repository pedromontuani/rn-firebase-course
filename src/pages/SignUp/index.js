import React, { useState } from 'react';
import {
  Alert,
  Text,
  View,
  TextInput,
  ImageBackground,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import { Spinner } from 'native-base';

import styles from './styles';
import colors from '../../theme/colors';
import RoundButton from '../../components/RoundButton';
import {
  emailAndPasswordSignUp,
  getCurrentUser
} from '../../services/authService';
import { listenNotifications } from '../../services/notificationsService';

const SignUp = props => {
  const [avatar, setAvatar] = useState(undefined);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const imagePickerOptions = {
    mediaType: 'photo',
    maxWidth: 500,
    maxHeight: 500,
    rotation: 0,
    noData: true
  };

  const selectAvatar = () => {
    ImagePicker.launchImageLibrary(imagePickerOptions, res => {
      setAvatar(res);
    });
  };

  const signUp = () => {
    setLoading(true);
    emailAndPasswordSignUp(name, email, password, avatar)
      .then(() => {
        const uid = getCurrentUser().uid;
        listenNotifications(uid);
        setLoading(false);
        props.navigation.navigate('User');
      })
      .catch(err => {
        console.log(JSON.stringify(err));
        if (err.code == 'auth/email-already-in-use') {
          Alert.alert('Erro', 'Email já cadastrado');
        } else {
          Alert.alert('Erro', 'Ocorreu um erro. Tente novamente.');
        }
        setLoading(false);
      });
  };

  return (
    <ImageBackground
      style={styles.viewport}
      source={require('../../theme/assets/login-background/background.png')}
      resizeMode={'cover'}
    >
      <KeyboardAvoidingView
        behavior={'position'}
        contentContainerStyle={styles.container}
        style={styles.kbAvoindingView}
      >
        <TouchableOpacity onPress={selectAvatar} style={styles.avatarHolder}>
          <Image
            style={styles.avatar}
            source={
              avatar && avatar.uri
                ? { uri: avatar.uri }
                : require('../../theme/assets/no-photo.jpg')
            }
            resizeMode={'cover'}
          />
          <Icon name="plus-circle" style={styles.avatarSelection} />
        </TouchableOpacity>

        <View style={styles.inputsHolder}>
          <TextInput
            onChangeText={text => setName(text)}
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Nome'}
            autoCompleteType={'name'}
            textContentType={'name'}
            autoCapitalize={'words'}
          ></TextInput>
          <TextInput
            onChangeText={text => setEmail(text)}
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Email'}
            autoCompleteType={'email'}
            keyboardType={'email-address'}
            textContentType={'emailAddress'}
            autoCapitalize={'none'}
          ></TextInput>
          <TextInput
            onChangeText={text => setPassword(text)}
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Senha'}
            textContentType={'password'}
            secureTextEntry={true}
          ></TextInput>
          <TextInput
            onChangeText={text => setVerifyPassword(text)}
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Repetir senha'}
            textContentType={'password'}
            secureTextEntry={true}
          ></TextInput>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.buttonsHolder}>
        <RoundButton
          backgroundColor={'rgba(255, 255, 255, 1)'}
          borderColor={'transparent'}
          onPress={signUp}
        >
          {loading ? (
            <Spinner color={colors.primary} />
          ) : (
            <Text style={styles.textFullButton}>CADASTRE-SE</Text>
          )}
        </RoundButton>
        <RoundButton
          backgroundColor={'transparent'}
          borderColor={'transparent'}
          onPress={() => {
            props.navigation.goBack();
          }}
        >
          <Text style={styles.textOutlineButton}>
            Já possui uma conta?
            <Text style={{ fontWeight: 'bold' }}> Faça o login!</Text>
          </Text>
        </RoundButton>
      </View>
    </ImageBackground>
  );
};

export default SignUp;
