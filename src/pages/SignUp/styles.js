import { StyleSheet, Dimensions } from 'react-native';
import variables from '../../theme/variables';
import colors from '../../theme/colors';

const deviceHeigth = Dimensions.get('window').height - 20;
const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  viewport: {
    height: deviceHeigth,
    paddingHorizontal: variables.pagePadding,
    paddingVertical: 5
  },
  kbAvoindingView: {
    flex: 0.8
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignSelf: 'stretch'
  },
  avatarHolder: {
    flex: 0.3,
    justifyContent: 'flex-start',
    alignSelf: 'center'
  },
  avatar: {
    height: deviceHeigth * 0.25,
    width: deviceHeigth * 0.25,
    borderRadius: deviceHeigth * 0.25,
    borderWidth: 6,
    borderColor: '#FFF'
  },
  avatarSelection: {
    fontSize: 26,
    color: '#FFF',
    alignSelf: 'flex-end',
    marginTop: -20
  },
  inputsHolder: {
    flex: 0.5,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  inputOuter: {
    paddingVertical: 10
  },
  textInput: {
    width: '100%',
    color: '#FFFFFF'
  },
  buttonsHolder: {
    flex: 0.2,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    paddingTop: 20
  },
  textFullButton: {
    fontWeight: 'bold',
    color: colors.primary
  },
  textOutlineButton: {
    color: '#FFFFFF'
  },
  socialIcon: {
    fontSize: 22
  }
});
