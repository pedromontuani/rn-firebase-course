import React, { useState, useEffect } from 'react';
import { View, FlatList, TextInput } from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Title
} from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';

import styles from './styles';
import colors from '../../theme/colors';
import ChatItem from '../../components/ChatItem';

import { getContacts } from '../../services/firestoreService';
import { getCurrentUser } from '../../services/authService';

const Contacts = props => {
  const [refreshing, setRefreshing] = useState(true);
  const [contacts, setContacts] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    loadContacts();
  }, []);

  const loadContacts = () => {
    setRefreshing(true);
    getContacts()
      .then(contacts => {
        setContacts(
          contacts.docs.filter(doc => doc.id != getCurrentUser().uid)
        );
        setRefreshing(false);
      })
      .catch(err => {
        console.log('Error getting contacts', err);
        setRefreshing(false);
      });
  };

  const normalizeText = str => {
    str = str.replace(/[ÀÁÂÃÄÅ]/, 'A');
    str = str.replace(/[àáâãäå]/, 'a');
    str = str.replace(/[ÈÉÊË]/, 'E');
    str = str.replace(/[èéêë]/, 'e');
    str = str.replace(/[ÍÌÏ]/, 'I');
    str = str.replace(/[íìï]/, 'i');
    str = str.replace(/[ÔÕÖ]/, 'O');
    str = str.replace(/[õôö]/, 'ô');
    str = str.replace(/[ÚÜ]/, 'U');
    str = str.replace(/[úü]/, 'u');
    str = str.replace(/[Ç]/, 'C');
    str = str.replace(/[ç]/, 'c');
    str.replace(/[^a-z0-9]/gi, '');

    return str.toLowerCase();
  };

  const filterByText = contactName => {
    if (searchTerm.length > 0) {
      const name = normalizeText(contactName);
      const search = normalizeText(searchTerm);
      return name.indexOf(search) > -1;
    } else {
      return true;
    }
  };

  return (
    <Container>
      <Header androidStatusBarColor={colors.primaryDark} style={styles.header}>
        <Left>
          <TouchableNativeFeedback>
            <Button transparent onPress={() => props.navigation.goBack()}>
              <Icon name={'arrow-back'} />
            </Button>
          </TouchableNativeFeedback>
        </Left>
        <Body>
          <Title>Contatos</Title>
        </Body>
        <Right />
      </Header>

      <View style={styles.content}>
        <View style={styles.searchboxOuter}>
          <View style={styles.searchboxInner}>
            <View style={styles.searchIconHolder}>
              <Icon
                name={'search'}
                type={'FontAwesome'}
                style={styles.searchIcon}
              />
            </View>
            <TextInput
              placeholder={'Pesquisar...'}
              style={styles.searchInput}
              onChangeText={text => setSearchTerm(text)}
            />
          </View>
        </View>
        <FlatList
          contentContainerStyle={styles.contactsList}
          data={contacts}
          refreshing={refreshing}
          onRefresh={loadContacts}
          keyExtractor={item => item.id}
          renderItem={({ item }) =>
            filterByText(item.data().name) && (
              <ChatItem
                avatar={item.data().avatar}
                name={item.data().name}
                onPress={() => {
                  props.navigation.navigate('Chat', {
                    contactInfo: {
                      uid: item.id,
                      avatar: item.data().avatar,
                      name: item.data().name
                    }
                  });
                }}
              />
            )
          }
        />
      </View>
    </Container>
  );
};

export default Contacts;
