import React, { useState, useEffect } from 'react';
import { View, FlatList, TextInput, ImageBackground } from 'react-native';
import {
  Container,
  Header,
  Footer,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Title,
  Thumbnail
} from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import moment from 'moment';

import styles from './styles';
import colors from '../../theme/colors';
import Message from '../../components/Message';
import { getCurrentUser } from '../../services/authService';
import { getMessages, sendMessage } from '../../services/firestoreService';
import { sendNotification } from '../../services/notificationsService';

const Chat = props => {
  const [messages, setMessages] = useState([]);
  const [message, setMessage] = useState('');
  const [contactInfo, setContactInfo] = useState(
    props.navigation.getParam('contactInfo', undefined)
  );
  const userUid = getCurrentUser().uid;
  let unsubscribe = undefined;

  useEffect(() => {
    unsubscribe = getMessages(userUid, contactInfo.uid).onSnapshot(msgs => {
      setMessages(msgs.docs);
    });
  }, []);

  useEffect(() => {
    return () => {
      unsubscribe();
    };
  }, []);

  const SendMessage = () => {
    const msgBackup = message;
    sendMessage(userUid, contactInfo.uid, message)
      .then(() => {
        sendNotification(userUid, contactInfo.uid, message);
      })
      .catch(err => {
        console.log('Sending message error', err);
      });
    setMessage('');
  };

  const extractTimestamp = timestamp => {
    const fullTime = timestamp.toDate();

    if (new Date().toDateString() == fullTime.toDateString()) {
      return moment(fullTime).format('HH:mm');
    } else {
      return moment(fullTime).format('DD/MM/YY HH:mm');
    }
  };

  return (
    <ImageBackground
      source={require('../../theme/assets/chat-background/background.png')}
      style={styles.imageBackground}
    >
      <Container style={styles.container}>
        <Header
          androidStatusBarColor={colors.primaryDark}
          style={styles.header}
        >
          <Left>
            <TouchableNativeFeedback>
              <Button transparent onPress={() => props.navigation.goBack()}>
                <Icon name={'arrow-back'} />
              </Button>
            </TouchableNativeFeedback>
          </Left>
          <Body style={styles.contactInfo}>
            <Thumbnail
              style={styles.contactAvatar}
              source={
                contactInfo.avatar
                  ? { uri: contactInfo.avatar }
                  : require('../../theme/assets/no-photo.jpg')
              }
            />
            <Title style={styles.contactName}>{contactInfo.name}</Title>
          </Body>
          <Right />
        </Header>

        <View style={styles.content}>
          <FlatList
            inverted
            data={messages}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (
              <Message
                userUid={userUid}
                senderUid={item.data().senderUid}
                message={item.data().message}
                timestamp={extractTimestamp(item.data().timestamp)}
              />
            )}
          />
        </View>

        <Footer style={styles.footer}>
          <Body style={styles.inputHolder}>
            <View style={styles.inputOuter}>
              <TextInput
                placeholder={'Digite aqui...'}
                value={message}
                onChangeText={text => setMessage(text)}
              />
            </View>
          </Body>
          <Right style={styles.buttonHolder}>
            <Button style={styles.sendButton} onPress={SendMessage}>
              <Icon
                name={'send'}
                type={'MaterialIcons'}
                style={styles.sendIcon}
              />
            </Button>
          </Right>
        </Footer>
      </Container>
    </ImageBackground>
  );
};

export default Chat;
