import firebase from 'react-native-firebase';
import { getContact } from './firestoreService';

const listenNotifications = userUid => {
  return checkPermission().then(enabled => {
    if (enabled) {
      console.log('Notifications permissions enabled');
      getToken().then(token => {
        console.log('Got device notification token', token);
        notificationsListener(userUid);
      });
    } else {
      return requestPermission();
    }
  });
};

const checkPermission = () => {
  return firebase.messaging().hasPermission();
};

const getToken = () => {
  return firebase.messaging().getToken();
};

const requestPermission = () => {
  return firebase
    .messaging()
    .requestPermission()
    .then(() => {
      console.log('User granted notification permission');
    })
    .catch(error => {
      console.log("User didn't allow notification permission");
    });
};

const notificationsListener = userUid => {
  setNotificationChannel(userUid);
  return firebase.notifications().onNotification(notification => {
    console.log(notification.data);
    notification.android.setChannelId('messages');
    notification.android.setLargeIcon(notification.data.icon);
    firebase
      .notifications()
      .displayNotification(notification)
      .catch(err => {
        console.log('Display notification error', err);
      });
  });
};

const setNotificationChannel = userUid => {
  const channel = new firebase.notifications.Android.Channel(
    'messages',
    'Mensagens',
    firebase.notifications.Android.Importance.Max
  );
  firebase.notifications().android.createChannel(channel);
  firebase.messaging().subscribeToTopic(userUid);
};

const sendNotification = (senderUid, contactUid, message) => {
  getContact(senderUid).then(contactInfo => {
    const notification = JSON.stringify({
      to: `/topics/${contactUid}`,
      notification: {
        title: contactInfo.data().name,
        body: message
      },
      data: {
        icon: contactInfo.data().avatar
      }
    });
    return fetch('https://fcm.googleapis.com/fcm/send', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'key=AAAAKeNIXBw:APA91bHcw8Si12GPk5VLz7-SowFEa9-kNq0s1nO53GaCDJvetOPsPc4u2Y5mnvphAbveHEn-gYuVhaFE2YtDQL5l1_LiQHFH9943S_xgo6jI4bcZrbznwV29NTD6fYNz5cDMbsRRGHb5'
      },
      body: notification
    })
      .then(res => {
        console.log('Notification POST', res);
      })
      .catch(err => {
        console.log('Notification POST error', err);
      });
  });
};

export { listenNotifications, sendNotification };
