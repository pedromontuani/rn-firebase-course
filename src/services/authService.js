import firebase from 'react-native-firebase';
import { GoogleSignin } from '@react-native-community/google-signin';
import { saveFile } from './storageService';

const getAuth = () => {
  return firebase.auth();
};

const getCurrentUser = () => {
  return firebase.auth().currentUser;
};

const emailAndPasswordSignUp = (name, email, password, photo) => {
  return firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(data => {
      console.log('User created', data);
      const documentReference = firebase
        .firestore()
        .collection('users')
        .doc(data.user.uid);
      return documentReference
        .set({
          name: name
        })
        .then(() => {
          console.log('User data setted');
          if (photo) {
            return saveFile(`users/${data.user.uid}/avatar.jpg`, photo.uri, {
              contentType: photo.type
            }).then(fileObject => {
              console.log('File saved');
              documentReference
                .update({
                  avatar: fileObject.downloadURL
                })
                .then(() => {
                  console.log('User data updated');
                });
            });
          }
        });
    });
};

const emailAndPasswordSignIn = (email, password) => {
  return firebase.auth().signInWithEmailAndPassword(email, password);
};

const googleSignIn = () => {
  GoogleSignin.configure({
    webClientId:
      '179906829340-bcubqhfjlogm0e9gqgnatehrbm60gbap.apps.googleusercontent.com'
  });

  return GoogleSignin.signIn().then(data => {
    //faz o login no google
    console.log('User logged in', data);
    return GoogleSignin.getTokens().then(tokens => {
      //obtem os tokens de acesso
      console.log('User got tokens', tokens);
      const credential = firebase.auth.GoogleAuthProvider.credential(
        tokens.idToken,
        tokens.accessToken
      );
      //faz o login na aplicação
      return firebase
        .auth()
        .signInWithCredential(credential)
        .then(userData => {
          console.log('User data', userData);
          return firstAcessVerification(
            userData.user.uid,
            userData.user.displayName,
            userData.user.photoURL
          );
        });
    });
  });
};

const firstAcessVerification = (userUid, displayName, photoUrl) => {
  const userReference = firebase
    .firestore()
    .collection('users')
    .doc(userUid);
  return userReference.get().then(userObject => {
    if (!userObject.data()) {
      return userReference.set({
        name: displayName,
        avatar: photoUrl
      });
    } else {
      return true;
    }
  });
};

const signOut = () => {
  return firebase.auth().signOut();
};

export {
  getAuth,
  getCurrentUser,
  emailAndPasswordSignIn,
  emailAndPasswordSignUp,
  googleSignIn,
  signOut
};
