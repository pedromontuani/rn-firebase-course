import firebase from 'react-native-firebase';

const saveFile = (storageRef, filePath, metadata) => {
  return firebase
    .storage()
    .ref(storageRef)
    .putFile(filePath, metadata);
};

export { saveFile };
