import firebase from 'react-native-firebase';

const getContacts = userUid => {
  return firebase
    .firestore()
    .collection('users')
    .orderBy('name')
    .get();
};

const getContact = contactUid => {
  return firebase
    .firestore()
    .collection('users')
    .doc(contactUid)
    .get();
};

const getMessages = (userUid, contactUid) => {
  return firebase
    .firestore()
    .collection('users')
    .doc(userUid)
    .collection('chats')
    .doc(contactUid)
    .collection('messages')
    .orderBy('timestamp', 'desc');
};

const getChats = userUid => {
  return firebase
    .firestore()
    .collection('users')
    .doc(userUid)
    .collection('chats')
    .orderBy('lastMessage.timestamp', 'desc');
};

const sendMessage = (userUid, contactUid, message) => {
  const msg = {
    message: message,
    senderUid: userUid,
    timestamp: new Date()
  };

  return firebase
    .firestore()
    .collection('users')
    .doc(contactUid)
    .collection('chats')
    .doc(userUid)
    .collection('messages')
    .add(msg)
    .then(messageObj => {
      return firebase
        .firestore()
        .collection('users')
        .doc(userUid)
        .collection('chats')
        .doc(contactUid)
        .collection('messages')
        .doc(messageObj.id)
        .set(msg)
        .then(() => {
          return setLastMessage(userUid, contactUid, msg);
        });
    });
};

const setLastMessage = (userUid, contactUid, message) => {
  return firebase
    .firestore()
    .collection('users')
    .doc(contactUid)
    .collection('chats')
    .doc(userUid)
    .set({ lastMessage: message })
    .then(() => {
      return firebase
        .firestore()
        .collection('users')
        .doc(userUid)
        .collection('chats')
        .doc(contactUid)
        .set({ lastMessage: message });
    });
};

export { getContacts, getContact, getMessages, sendMessage, getChats };
