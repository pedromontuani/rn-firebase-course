import React from 'react';
import { StatusBar } from 'react-native';
import { Root } from 'native-base';

import Routes from './routes';
import colors from './theme/colors';

const App = () => (
  <Root>
    <StatusBar barStyle="light-content" backgroundColor={colors.primaryDark} />
    <Routes />
  </Root>
);

export default App;
