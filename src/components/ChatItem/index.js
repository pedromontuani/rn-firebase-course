import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import {
  ListItem,
  Left,
  Thumbnail,
  Icon,
  Body,
  Right,
  Text as StyledText
} from 'native-base';

import styles from './styles';
import { getContact } from '../../services/firestoreService';

const ChatItem = ({
  contactUid,
  avatar,
  name,
  subtitle,
  timestamp,
  noBorder = false,
  onPress,
  props
}) => {
  const [contactInfo, setContactInfo] = useState({ name: '', avatar: '' });

  const navigateToChat = () => {
    if (props) {
      props.navigation.navigate('Chat', {
        contactInfo: {
          uid: contactUid,
          avatar: contactInfo.avatar,
          name: contactInfo.name
        }
      });
    }
  };

  useEffect(() => {
    if (contactUid) {
      getContact(contactUid)
        .then(contact => {
          setContactInfo(contact.data());
        })
        .catch(err => {
          console.log('Getting contact info failed', err);
        });
    }
  }, []);

  return (
    <View style={styles.itemHolder}>
      <ListItem
        delayPressIn={0}
        delayPressOut={200}
        avatar
        noBorder={noBorder}
        style={styles.listItem}
        onPress={onPress ? onPress : navigateToChat}
      >
        <Left>
          <Thumbnail
            source={
              avatar || contactInfo.avatar
                ? { uri: avatar ? avatar : contactInfo.avatar }
                : require('./../../theme/assets/no-photo.jpg')
            }
            style={styles.avatar}
          />
        </Left>
        <Body>
          {subtitle ? (
            <View>
              <Text numberOfLines={1} style={styles.contactName}>
                {name ? name : contactInfo.name}
              </Text>
              <Text numberOfLines={1} style={styles.subtitle}>
                {subtitle}
              </Text>
            </View>
          ) : (
            <Text
              numberOfLines={1}
              style={[styles.contactName, styles.textSpacing]}
            >
              {name}
            </Text>
          )}
        </Body>
        <Right>{timestamp && <StyledText note>{timestamp}</StyledText>}</Right>
      </ListItem>
    </View>
  );
};

export default ChatItem;
