export default {
  primary: '#5F49AB',
  primaryDark: '#2D207B',
  secondary: '#7058C0'
};
